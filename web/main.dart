import 'engine/canvas.dart';
import 'mainShared.dart';
import 'game/player.dart';
import 'dart:async';

void main() {
   changeRoom(0);
   Timer.periodic(Duration(milliseconds: 50), (Timer timer) {
      if (player.x + player.width > canvas.width) {
         changeRoom(roomIndex + 1);
      } else if (player.y + player.height > canvas.height) {
         ++player.deaths;
         changeRoom(roomIndex);
      }

      roomValue.update();
   });
}