import '../engine/Room.dart';
export '../engine/Room.dart';
import 'player.dart';
import 'Platform.dart';
import 'DeathCounter.dart';
import 'Loader.dart';
import 'Secret.dart';

final ROOMS = [
() => Room(false, '''

 LOADING...


















''', [Loader(1, 1)]),
() => Room(false, '''
############################################################
#                                                          #
# SUPER SECRET ROOM                                        #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#             0                                            #
#          COW                                           #
#          | |                                           #
############################################################
''', [player.reset(2, 17, false), Secret.bouncer(2, 4), Secret.face(20, 2), Secret.portal(58, 18, 6)]),
() => Room(false, '''













 THIS IS JACK

                                                        -->



#############-HE-CAN-MOVE-WITH-THE-ARROW-KEYS-##############
''', [player.reset(1, 17, false)]),
() => Room(false, '''











                             WITH-SPACE



                    CAN-JUMP



##########   AND-HE                               ##########
''', [player.reset(1, 17, false)]),
() => Room(false, '''










_________
         \\
          \\
           \\
-----------+\\
           |     HE   WAS   LEAVING   HOME   TO   STUDY
 HOME      |
      +-+  |
      | |  |
############################################################
''', [player.reset(1, 8, false)]),
() => Room(true, '''
                                                       +---+
 +--------+                                            |   |
 |        |                                            |   |
 | IT WAS |   +-----------+                            |   |
 |   HIS  |   |           |                            |   |
 |        |   | HOME      |   +-----------+            |   |
 |        |   |      AWAY |   |           |            |   |
 |        |   |           |   | FROM      |   +-----------+|
 |        |   |           |   |      HOME |   |           ||
 |        |   |           |   |           |   |           ||
 |        |   |           |   |           |   |           ||
 |        |   |           |   |           |   |           ||
 |        |   |           |   |           |   |           ||
############################################################





##########                                        ##########
''', [player.reset(1, 17, false), Platform(46, 19, 'TAXI')]),
() => Room(true, '''





                                                       +---+
                                                       |   |
 +--------+                                            |   |
 |        |                                            |   |
 | AT     |   +-----------+                            |   |
 |  FIRST |   |           |                            |   |
 | IT     |   | LIKE A    |   +-----------+            |   |
 |   FELT |   |   HOLIDAY |   |           |            |   |
 |        |   |           |   | BUT HE    |   +-----------+|
 |        |   |           |   |      KNEW |   |           ||
 |        |   |           |   |           |   | IT        ||
 |        |   |           |   |           |   |    WASN'T ||
 |        |   |           |   |           |   |           ||
 |        |   |           |   |           |   |           ||
/                                                          |
''', [player.reset(2, 5, false), Platform(2, 19, 'CAR'), Platform(50, 7, 'PLANE'), Secret.portal(0, 18, 1)]),
() => Room(true, '''

 IN HIS ACTUAL HOLIDAYS

         HE WOULD GO "HOME"...?









                         ==========
##########                                        ##########





''', [player.reset(1, 12, false), Platform(45, 14, 'PLANE')]),
() => Room(true, '''

 IT LOGICALLY MADE SENSE

   IT JUST FELT A BIT BACKWARS 


                             ++                   +--------+
                             ||                   |        |
                             ||                   |        |
                             ||                   |        |
                             :;                   |        |
                                                  |        |
                                                  :        |
                                                  ,        |
---------+                                                 |
         |                                                _/
         !



''', [player.reset(1, 12, false), Platform(54, 14, 'PLANE'), Platform(31, 10, 'PLANE')]),
() => Room(false, '''
############################################################
      | |  |
      +-+  |   "HOME" NOW FELT A BIT DIFFERENT
 HOWE      |
           |
-----------+/
           /
                                            \\
                                            |
--------------                              |
                                            |
                                            |
                                            |
                 MOST THINGS STAYED THE SAME/


BUT THAT MADE THE DIFFERENT THINGS STAND OUT               #



''', [player.reset(1, 7, false), Platform(44, 16, '+--+')]),
() => Room(true, '''



















########   WHAT   EVEN   IS   HOME   FOR    JACK?   ########
''', [player.reset(1, 17, false)]),
() => Room(true, '''





                                                       +---+
                                                       |   |
 +--------+                                            |   |
 |        |                                            |   |
 | I      |   +-----------+                            |   |
 |  GUESS |   |    IS HIS |   HOME                     |   |
 |   THIS |   +-----------+   +-----------+            |   |
 |        |                   +----------+             |   |
 +-------+                                     +----------+|
                                                           <
              +-----------+                                <
              |           |   +-----------+                <
 +--------+   |           |   |       NOW |   +-----------++
 |        |   |           |   |           |   |           ||
/                                                          |
''', [player.reset(2, 15, false), Platform(2, 19, 'CAR'), Platform(50, 7, 'PLANE')]),
() => Room(true, '''

 IT WILL TAKE TIME TO ADJUST

    IT WILL BE TOUGH

      BUT HE WILL MAKE IT
                                        ++        +--------+
                                        ||        |        |
                                        ||        |        |
                                        ||        +-----\\ <
                                        :;              |  |
                                                  ^-----/  |
                                                           |
                                                           |
---------+                                                 |
         |                                                _/
         !



''', [player.reset(1, 12, false), Platform(10, 14, 'BUS'), Platform(54, 14, 'TRUCK'), Platform(43, 10, 'PLANE')]),
() => Room(false, '''

 BECAUSE THIS IS HIS HOME NOW


                       (MUSIC: STARLING BY SOUND OF PICTURE)









---------+
         |
         |
         |                             (THANKS FOR PLAYING)
         |                              (YOU DIED _ TIME)
         |
''', [player.reset(1, 12, false), DeathCounter(40, 18)]),
() => Room(false, '''
############################################################
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
#                                                          #
############################################################
''', [player.reset(2, 17, false)]),
];