import '../engine/Entity.dart';
import 'player.dart';

class Platform extends Entity {
   num _dx = 0.8;

   Platform(num x, num y, String text) : super(x, y, text.length, 1, text);

   @override
   void update() {
      num origX = x;
      if (move(_dx, 0)) {
         _dx = -_dx;
      }

      if (player.x < x + width && player.x + player.width > x) {
         if (player.y + player.height == y) {
            player.move(x - origX, 0);
         }
      }
   }
}