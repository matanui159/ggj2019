import '../engine/Entity.dart';
import '../engine/input.dart';

class Player extends Entity {
   bool tall = false;
   bool _ground = false;
   num _dy = 0;
   int deaths = 0;

   Player._() : super(0, 0, 1, 2, 'J\nK');

   Player reset(num x, num y, bool tall) {
      this.x = x;
      this.y = y;
      this.tall = tall;
      if (tall) {
         height = 3;
         text = 'J\nA\nK';
      } else {
         height = 2;
         text = 'J\nK';
      }
      _dy = 0;
      _ground = false;
      return this;
   }

   @override
   void update() {
      num dx = 0;
      if (input.buttonDown(Button.left)) {
         --dx;
      }
      if (input.buttonDown(Button.right)) {
         ++dx;
      }
      player.move(dx * 0.9, 0);
      if (x < 0) {
         x = 0;
      }

      if (_ground) {
         if (input.buttonDown(Button.jump)) {
            _dy = -1.5;
         }
      }
      _dy += 0.2;
      _ground = false;
      if (player.move(0, _dy)) {
         if (_dy > 0) {
            _ground = true;
         }
         _dy = 0;
      }
      if (y < 0) {
         y = 0;
         _dy = 0;
      }
   }
}

final player = Player._();