import '../engine/Entity.dart';
import '../mainShared.dart';
import 'dart:async';
import 'dart:html';

class Loader extends Entity {
   Loader(num x, num y) : super(x, y, 1, 1, '') {
      var music = AudioElement();

      void loaded(Event event) {
         text = 'PRESS ANY KEY TO CONTINUE';
         draw();

         window.onKeyDown.first.then((event) {
            music.onEnded.listen((event) {
               Timer(Duration(seconds: 3), () => music.play());
            });

            music.play();
            changeRoom(2);
         });
      }

      music.onCanPlayThrough.first.then(loaded);
      music.onError.first.then(loaded);
      music.src = 'music.mp3';
   }

   @override
   void update() {}
}