import '../engine/Entity.dart';
import 'player.dart';
import '../mainShared.dart';

abstract class Secret extends Entity {
   Secret._(num x, num y, num width, num height, String text)
      : super(x, y, width, height, text);

   factory Secret.portal(num x, num y, int room) {
      return _Portal(x, y, room);
   }

   factory Secret.bouncer(num x, num y) {
      return _Bouncer(x, y);
   }

   factory Secret.face(num x, num y) {
      return _Face(x, y);
   }
}

class _Portal extends Secret {
   int _room;

   _Portal(num x, num y, this._room) : super._(x, y, 1, 1, '');

   @override
   void update() {
      if (player.x.truncate() == x && player.y.truncate() == y - 1) {
         changeRoom(_room);
      }
   }
}

class _Bouncer extends Secret {
   num _dx = 0.5;
   num _dy = 0.5;

   _Bouncer(num x, num y) : super._(x, y, 3, 1, 'DVD');

   @override
   void update() {
      if (move(_dx, 0)) {
         _dx = -_dx;
      }
      if (move(0, _dy)) {
         _dy = -_dy;
      }
   }
}

class _Face extends Secret {
   num timer = 0;

   _Face(num x, num y) : super._(x, y, 3, 1, 'OwO');

   @override
   void update() {
      ++timer;
      if (timer == 20) {
         timer = 0;
      }
      if (timer < 10) {
         text = 'OwO';
      } else {
         text = 'UwU';
      }
      draw();
   }
}