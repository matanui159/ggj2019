import '../engine/Entity.dart';
import 'player.dart';

class DeathCounter extends Entity {
   DeathCounter(num x, num y) : super(x, y, 1, 1, '');

   @override
   void update() {
      var s = 'S';
      if (player.deaths == 1) {
         s = '';
      }
      text = '(YOU DIED ${player.deaths} TIME${s})';
      draw();
   }
}