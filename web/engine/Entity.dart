import 'canvas.dart';
import 'dart:math';

abstract class Entity {
   num x;
   num y;
   num width;
   num height;
   String text;

   Entity(this.x, this.y, this.width, this.height, this.text);

   bool move(num dx, num dy) {
      if (dx != 0 && dy != 0) {
         bool collideX = move(dx, 0);
         bool collideY = move(0, dy);
         return collideX || collideY;
      }
      canvas.clear(x, y, width, height);
      x += dx;
      y += dy;

      bool collide = false;
      canvas.forEach((num cx, num cy) {
         // if (cx >= trunX && cx < trunX + width && cy >= trunY && cy < trunY + height) {
         //    return;
         // }
         String cell = canvas.getCell(cx, cy);
         if (cell == ' ') {
            return;
         }

         Point pos = canvas.getRealPosition(cx, cy);
         if (x < pos.x + 1 && x + width > pos.x) {
            if (y < pos.y + 1 && y + height > pos.y) {
               // if (cell == 'J' || cell == 'K') {
               //    print('COLLIDE');
               // }
               if (dx < 0) {
                  x = pos.x + 1;
               } else if (dx > 0) {
                  x = pos.x - width;
               } else if (dy < 0) {
                  y = pos.y + 1;
               } else {
                  y = pos.y - height;
               }

               collide = true;
            }
         }
      });

      draw();
      return collide;
   }

   void draw() {
      canvas.draw(text, x, y);
   }

   void update();
}