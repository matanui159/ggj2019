import 'canvas.dart';
import 'Entity.dart';

class Room {
   String _map;
   List<Entity> _entities;

   Room(bool invert, this._map, this._entities) {
      if (invert) {
         canvas
            ..background = 'black'
            ..foreground = 'white';
      } else {
         canvas
            ..background = 'white'
            ..foreground = 'black';
      }

      canvas.clear(0, 0, canvas.width, canvas.height);
      canvas.draw(_map);
      for (var entity in _entities) {
         entity.draw();
      }
   }

   void update() {
      for (var entity in _entities) {
         entity.update();
      }
   }
}