import 'dart:html';

class _Cell {
   String text = ' ';
   var position = Point<num>(0, 0);
}

class Canvas {
   final width = 60;
   final height = 20;
   final fontSize = 32;
   int fontWidth;
   int fontHeight;
   CanvasRenderingContext2D _ctx;
   var _cells = <_Cell>[];
   String _fg = 'black';

   set background(String bg) => document.body.style.backgroundColor = bg;
   set foreground(String fg) {
      _ctx.canvas.style.borderColor = fg;
      _fg = fg;
   }

   Canvas._() {
      fontWidth = (fontSize * 0.6).round();
      fontHeight = (fontSize * 0.9).round();

      CanvasElement canvas = querySelector('#canvas');
      canvas.width = width * fontWidth;
      canvas.height = height * fontHeight;
      _ctx = canvas.getContext('2d');
      _ctx
         ..font = '${fontSize}px monospace'
         ..textAlign = 'left'
         ..textBaseline = 'top';

      forEach((x, y) => _cells.add(_Cell()));
      _requestDraw();
   }

   void forEach(void callback(num x, num y)) {
      for (var y = 0; y < height; ++y) {
         for (var x = 0; x < width; ++x) {
            callback(x, y);
         }
      }
   }

   _Cell _getCell(num x, num y) {
      return _cells[y.truncate() * width + x.truncate()];
   }

   String getCell(num x, num y) {
      return _getCell(x, y).text;
   }

   void setCell(num x, num y, String text) {
      _getCell(x, y)
         ..text = text
         ..position = Point(x, y);
   }

   Point getRealPosition(num x, num y) {
      return _getCell(x, y).position;
   }

   void draw(String text, [num x = 0, num y = 0]) {
      num origX = x;
      for (var c in text.runes) {
         var str = String.fromCharCode(c);
         if (str == '\n') {
            x = origX;
            ++y;
         } else {
            setCell(x, y, str);
            ++x;
         }
      }
   }

   void clear(num x, num y, num width, num height) {
      for (num yy = y; yy < y + height; ++yy) {
         for (num xx = x; xx < x + width; ++xx) {
            setCell(xx, yy, ' ');
         }
      }
   }

   void _requestDraw() {
      window.requestAnimationFrame((dt) => _draw());
   }

   void _draw() {
      _ctx.clearRect(0, 0, _ctx.canvas.width, _ctx.canvas.height);
      _ctx.fillStyle = _fg;
      forEach((x, y) {
         var cell = _getCell(x, y);
         if (cell.text != ' ') {
            _ctx.fillText(
               cell.text,
               cell.position.x * fontWidth,
               cell.position.y * fontHeight
            );
         }
      });
      _requestDraw();
   }
}

final canvas = Canvas._();