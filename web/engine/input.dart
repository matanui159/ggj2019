import 'input/KeyboardInput.dart';
import 'input/GamepadInput.dart';
import 'input/GroupInput.dart';
export 'input/Input.dart';

final input = GroupInput([KeyboardInput(), GamepadInput()]);