import 'dart:html';

// https://keycode.info
class Keyboard {
   var _keys = <String, bool>{};

   Keyboard._() {
      window.onKeyDown.listen((event) => _keys[event.key] = true);
      window.onKeyUp.listen((event) => _keys.remove(event.key));
   }

   bool keyDown(String name) {
      return _keys[name] ?? false;
   }
}

final keyboard = Keyboard._();