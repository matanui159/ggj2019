import 'Input.dart';
import 'dart:html';

class GamepadInput implements Input {
   var _left = false;
   var _right = false;
   var _jump = false;

   void _update() {
      _left = false;
      _right = false;
      _jump = false;

      for (var gamepad in window.navigator.getGamepads()) {
         if (gamepad != null && gamepad.mapping == 'standard') {
            if (gamepad.axes[0] < -0.2 || gamepad.axes[2] < -0.2
                  || gamepad.buttons[14].pressed) {
               _left = true;
            }

            if (gamepad.axes[0] > 0.2 || gamepad.axes[2] > 0.2
                  || gamepad.buttons[15].pressed) {
               _right = true;
            }

            for (int i = 0; i < 4; ++i) {
               if (gamepad.buttons[i].pressed) {
                  _jump = true;
               }
            }
         }
      }
   }

   @override
   bool buttonDown(Button button) {
      switch (button) {
         case Button.left:
            _update();
            return _left;
         case Button.right:
            return _right;
         case Button.jump:
            return _jump;
      }
      return false;
   }
}