import 'Input.dart';
import 'dart:html';

// https://keycode.info
class KeyboardInput implements Input {
   var _keys = <String, bool>{};

   KeyboardInput() {
      window.onKeyDown.listen((event) => _keys[event.key] = true);
      window.onKeyUp.listen((event) => _keys.remove(event.key));
   }

   bool _down(String key) {
      return _keys[key] ?? false;
   }

   bool buttonDown(Button button) {
      switch (button) {
         case Button.left:
            return _down('ArrowLeft') || _down('a');
         case Button.right:
            return _down('ArrowRight') || _down('d');
         case Button.jump:
            return _down('ArrowUp') || _down('w') || _down(' ');
      }
      return false;
   }
}