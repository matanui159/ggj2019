enum Button {
   left,
   right,
   jump
}

abstract class Input {
   bool buttonDown(Button button);
}