import 'Input.dart';

class GroupInput implements Input {
   List<Input> _inputs;

   GroupInput(this._inputs);

   @override
   bool buttonDown(Button button) {
      for (var input in _inputs) {
         if (input.buttonDown(button)) {
            print('${input} said ${button} was down');
            return true;
         }
      }
      return false;
   }
}